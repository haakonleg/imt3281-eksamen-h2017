# Før du begynner #

* Sørg for at repositoriet er privat og at jeg har lesetilgang.
* Rediger pom.xml filen og erstatt de to forekomstene av "Eksamen2017" med ditt studentnummer.
* Husk minst en commit pr. time utført arbeid, det er ikke påkrevet å pushe til bitbucket så ofte men det anbefales sterkt å gjøre det (det er din backup om maskinen din feiler.)

Oppgaveteksten ligger i [Wiki'en](https://bitbucket.org/okolloen/imt3281-eksamen-h2017/wiki/Home).

Lykke til.
