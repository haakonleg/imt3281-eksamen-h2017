package no.ntnu.imt3281.movieExplorer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * This class contains methods to query different search functions of TheMovieDB
 * API
 * 
 * @author Hakkon
 *
 */
public class Search {

    /**
     * Sends a multi search request:
     * https://developers.themoviedb.org/3/search/multi-search
     * 
     * @param query
     *            The search query. Will be converted to URI encoded string.
     * @return A JSON object which contains found results. The number of results can
     *         be found with the "results" value.
     * 
     */
    public static JSON multiSearch(String query) {
        try {
            query = URLEncoder.encode(query, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return API.sendRequest("search/multi?query=" + query + "&");
    }

    /**
     * Sends a search request for actors and crew which took part in a movie:
     * https://developers.themoviedb.org/3/movies/get-movie-credits. First attempts
     * to get the result from the database, and if not found, sends a request to
     * TheMovieDB API.
     * 
     * @param movieID
     *            The ID of the movie to query
     * @return A JSON object which contains found results.
     */
    public static JSON actors(long movieID) {
        JSON actors = fromCache("actors", movieID);
        if (actors == null) {
            actors = API.sendRequest("movie/" + Long.toString(movieID) + "/credits?");
            toCache("actors", movieID, actors);
        }
        return actors;
    }

    /**
     * Sends a search request for movies where a actor or crew too part in:
     * https://developers.themoviedb.org/3/discover/movie-discover. First attempts
     * to get the result from the database, and if not found, sends a request to
     * TheMovieDB API.
     * 
     * @param personID
     *            The ID of the actor or crew to query
     * @return A JSON object which contains found results. The number of movies can
     *         be found with the "total_results" value.
     */
    public static JSON takesPartIn(long personID) {
        JSON takesPartIn = fromCache("takesPartIn", personID);
        if (takesPartIn == null) {
            takesPartIn = API.sendRequest("discover/movie?sort_by=popularity.desc&with_people=" + Long.toString(personID) + "&");
            toCache("takesPartIn", personID, takesPartIn);
        }
        return takesPartIn;
    }

    /**
     * Sends a request to get details about a specific movie:
     * https://developers.themoviedb.org/3/movies/get-movie-details. First attempts
     * to get the result from the database, and if not found, sends a request to
     * TheMovieDB API.
     * 
     * @param movieID
     *            The ID of the movie to query
     * @return A JSON object which contains details about specified movie.
     */
    public static JSON movie(long movieID) {
        JSON movie = fromCache("movies", movieID);
        if (movie == null) {
            movie = API.sendRequest("movie/" + Long.toString(movieID) + "?");
            toCache("movies", movieID, movie);
        }
        return movie;
    }

    /**
     * Caches the result of a search query to the database
     * 
     * @param table
     *            The table to save the search query in
     * @param toCache
     *            The JSON object to cache
     */
    private static void toCache(String table, long id, JSON toCache) {
        Connection conn = Database.getConnection();
        String sql = "INSERT INTO " + table + " (id, json) VALUES (?, ?)";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.setString(2, toCache.toString());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Try to get the search result from database
     * 
     * @param table
     *            The table to get the search result from
     * @param id
     *            The id of the object to save
     * @return Returns the cached JSON object if it exists in the database,
     *         otherwise returns null
     */
    private static JSON fromCache(String table, long id) {
        HashMap<String, Object> result = Database.getColumn(table, Long.toString(id));

        if (result == null)
            return null;

        return new JSON((String) result.get("JSON"));
    }
}
