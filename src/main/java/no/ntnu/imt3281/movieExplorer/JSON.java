package no.ntnu.imt3281.movieExplorer;

import java.util.ArrayList;

import org.json.simple.*;
import org.json.simple.parser.*;

/**
 * This class is used to parse JSON objects and return their values.
 * 
 * @author Hakkon
 *
 */

public class JSON {
    private JSONParser parser;
    private JSONObject jsonObject;
    private JSONArray jsonArray;

    /**
     * Constructor for JSON object. Takes a JSON string and parses it. It detects if
     * the parsed json is a normal json object or a json array.
     * 
     * @param json
     */
    public JSON(String json) {
        this.parser = new JSONParser();

        try {
            Object parsed = parser.parse(json);
            if (parsed instanceof JSONObject) {
                this.jsonObject = (JSONObject) parsed;
            } else if (parsed instanceof JSONArray) {
                this.jsonArray = (JSONArray) parsed;
            } else {
                System.err.println("Not JSON");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    /**
     * Returns the value associated with the given key.
     * 
     * @param key
     *            The key the value maps to
     * @return Object with found value, null if the value was not found
     */
    public Object getValue(String key) {
        return jsonObject.get(key);
    }

    /**
     * Returns a new JSON object from given branch in the JSON structure
     * 
     * @param key
     *            The branch in JSON structure
     * @return New JSON object
     */
    public JSON get(String key) {
        Object obj = jsonObject.get(key);
        if (obj instanceof JSONArray) {
            return new JSON(((JSONArray) obj).toJSONString());
        } else if (obj instanceof JSONObject) {
            return new JSON(((JSONObject) obj).toJSONString());
        }
        return null;
    }

    /**
     * If this json is a json array, the method returns a new JSON object from array
     * index
     * 
     * Note: (JSON simple uses a hashmap if the JSON is not a json array, so it
     * makes no sense to use integer index if it is not a json array)
     * 
     * @param index
     *            The index in the array
     * @return The JSON object at specified array index
     */
    public JSON get(int index) {
        if (this.jsonArray != null) {
            return new JSON(this.jsonArray.get(index).toString());
        } else {
            System.err.println("Not a JSON array");
            return null;
        }
    }

    /**
     * Returns the number of elements in this JSON object
     * 
     * @return Number of elements in the JSON object or the JSON array
     */
    public int size() {
        if (this.jsonArray != null) {
            return jsonArray.size();
        }
        return jsonObject.size();
    }
    
    /**
     * Converts the JSONArray to an ArrayList of specified object
     * @return ArrayList of objects <T>
     */
    public <T> ArrayList<T> toArray () {
        ArrayList<T> arr = new ArrayList<>();
        
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.size(); i++) {
                arr.add((T) jsonArray.get(i));
            }
        }
        
        return arr;
    }
    
    /**
     * Returns the JSON encoded string of the JSON object
     */
    @Override
    public String toString() {
        if (this.jsonArray != null)
            return this.jsonArray.toJSONString();
        return this.jsonObject.toJSONString();
    }
}
