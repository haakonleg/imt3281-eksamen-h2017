package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.prefs.Preferences;

public class TheMovieDBConfiguration {

    private String baseURL, secureBaseURL;
    private ArrayList<String> backdropSizes, logoSizes, posterSizes, profileSizes, stillSizes;

    private String cacheDirectory;

    /**
     * No argument constructor tries to fetch the configuration from database first,
     * if it doesn't exist, it is fetched from API and then saved to the database.
     */
    public TheMovieDBConfiguration() {
        HashMap<String, Object> config = Database.getColumn("config", "0");

        if (config != null) { // In database
            this.baseURL = (String) config.get("BASEURL");
            this.secureBaseURL = (String) config.get("SECUREBASEURL");
            this.logoSizes = Util.stringToArray((String) config.get("LOGOSIZES"));
            this.posterSizes = Util.stringToArray((String) config.get("POSTERSIZES"));
            this.profileSizes = Util.stringToArray((String) config.get("PROFILESIZES"));
            this.stillSizes = Util.stringToArray((String) config.get("STILLSIZES"));
        } else { // Not in database
            this.readJSON(API.sendRequest("configuration?"));
            this.toDB();
        }

        // Get directory for image cache
        Preferences pref = Preferences.userRoot().node("MovieExplorer");
        this.cacheDirectory = pref.get("CacheDirectory", null);
    }

    /**
     * This is just a wrapper for the other constructor (used for the test)
     * 
     * @param json
     *            String of json encoded configuration data
     */
    public TheMovieDBConfiguration(String json) {
        this.readJSON(new JSON(json));
    }

    /**
     * Method that reads the configuration data from the JSON object into the class.
     * 
     * @param configData
     *            JSON object of TheMovieDB configuration data
     *            (https://developers.themoviedb.org/3/configuration/get-api-configuration)
     */
    private void readJSON(JSON configData) {
        JSON imageData = configData.get("images");
        this.baseURL = (String) imageData.getValue("base_url");
        this.secureBaseURL = (String) imageData.getValue("secure_base_url");

        this.backdropSizes = imageData.get("backdrop_sizes").toArray();
        this.logoSizes = imageData.get("logo_sizes").toArray();
        this.posterSizes = imageData.get("poster_sizes").toArray();
        this.profileSizes = imageData.get("profile_sizes").toArray();
        this.stillSizes = imageData.get("still_sizes").toArray();
    }

    /**
     * Saves the configuration data to database
     */
    private void toDB() {
        Connection conn = Database.getConnection();

        String sql = "INSERT INTO config"
                + "(id, baseURL, secureBaseURL, backdropSizes, logoSizes, posterSizes, profileSizes, stillSizes)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, 0);
            stmt.setString(2, this.baseURL);
            stmt.setString(3, this.secureBaseURL);
            stmt.setString(4, Util.arrayToString(this.backdropSizes));
            stmt.setString(5, Util.arrayToString(this.logoSizes));
            stmt.setString(6, Util.arrayToString(this.posterSizes));
            stmt.setString(7, Util.arrayToString(this.profileSizes));
            stmt.setString(8, Util.arrayToString(this.stillSizes));
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns largest image size from an array of image sizes
     * 
     * @param sizes
     *            An array of image sizes
     * @return The largest image size in array
     */
    private String getLargestImageSize(ArrayList<String> sizes) {
        String largest = sizes.get(0);
        int max = Integer.parseInt(largest.replaceAll("[^\\d.]", ""));
        for (int i = 1; i < sizes.size(); i++) {
            String sizeString = sizes.get(i);

            if (sizeString.equals("original"))
                continue;

            int sizeInt = Integer.parseInt(sizeString.replaceAll("[^\\d.]", ""));

            if (sizeInt > max) {
                max = sizeInt;
                largest = sizeString;
            }
        }
        return largest;
    }

    /**
     * Returns the backdrop image URL for specified image
     * 
     * @param image
     *            Name of the image
     * @return URL of the largest image size
     */
    public String getBackdropURL(String image) {
        String size = getLargestImageSize(this.backdropSizes);
        String imageURL = this.baseURL + size + "/" + image;

        String localImage = this.getImage(image, size);
        if (localImage == null) {
            this.cacheImage(imageURL, image, size);
            return imageURL;
        } else {
            return localImage;
        }
    }

    /**
     * Returns the logo image URL for specified image
     * 
     * @param image
     *            Name of the image
     * @return URL of the largest image size
     */
    public String getLogoURL(String image) {
        String size = getLargestImageSize(this.logoSizes);
        String imageURL = this.baseURL + size + "/" + image;

        String localImage = this.getImage(image, size);
        if (localImage == null) {
            this.cacheImage(imageURL, image, size);
            return imageURL;
        } else {
            return localImage;
        }
    }

    /**
     * Returns the poster image URL for specified image
     * 
     * @param image
     *            Name of the image
     * @return URL of the largest image size
     */
    public String getPosterURL(String image) {
        String size = getLargestImageSize(this.posterSizes);
        String imageURL = this.baseURL + size + "/" + image;

        String localImage = this.getImage(image, size);
        if (localImage == null) {
            this.cacheImage(imageURL, image, size);
            return imageURL;
        } else {
            return localImage;
        }
    }

    /**
     * Returns the profile image URL for specified image
     * 
     * @param image
     *            Name of the image
     * @return URL of the largest image size
     */
    public String getProfileURL(String image) {
        String size = getLargestImageSize(this.profileSizes);
        String imageURL = this.baseURL + size + "/" + image;

        String localImage = this.getImage(image, size);
        if (localImage == null) {
            this.cacheImage(imageURL, image, size);
            return imageURL;
        } else {
            return localImage;
        }
    }

    /**
     * Returns the still image URL for specified image
     * 
     * @param image
     *            Name of the image
     * @return URL of the largest image size
     */
    public String getStillURL(String image) {
        String size = getLargestImageSize(this.stillSizes);
        String imageURL = this.baseURL + size + "/" + image;

        String localImage = this.getImage(image, size);
        if (localImage == null) {
            this.cacheImage(imageURL, image, size);
            return imageURL;
        } else {
            return localImage;
        }
    }

    /**
     * Saves and image to the image cache directory. The code for downloading the
     * file has been taken from: https://stackoverflow.com/a/921400
     * 
     * @param imageURL
     *            The URL where this image exists on the internet
     * @param fileName
     *            The name of the JPG file
     * @param dir
     *            The subdirectory of the cache directory to save image to
     * @return True if the image was downloaded
     */
    private boolean cacheImage(String imageURL, String fileName, String dir) {
        if (this.cacheDirectory == null)
            return false;

        String output = this.cacheDirectory + "/" + dir + "/" + fileName;

        // https://stackoverflow.com/a/921400
        try {
            URL url = new URL(imageURL);
            ReadableByteChannel rbc = Channels.newChannel(url.openStream());
            FileOutputStream fos = new FileOutputStream(output);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            rbc.close();
        } catch (IOException e) {
            System.err.println("Could not save image");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * Tries to get the image from local filesystem/cache.
     * 
     * @param fileName
     *            The image filename
     * @param dir
     *            The directory to search in
     * @return Path to the image if it was found, null if no image was found.
     */
    private String getImage(String fileName, String dir) {
        if (this.cacheDirectory == null)
            return null;

        File image = new File(this.cacheDirectory + "/" + dir + "/" + fileName);

        if (image.exists())
            return "file:///" + image.getAbsolutePath();
        else
            return null;
    }

    /**
     * Returns the total size in KB of the specified image cache directory
     * 
     * @param dir
     *            Name of cache directory
     * @return Size in KB
     */
    public long sizeOfCache(String dir) {
        return Util.getDirectorySize(new File(this.cacheDirectory + "/" + dir));
    }

    /**
     * Empty the cache directory. Deletes all regular files in cache.
     */
    public void emptyCache() {
        if (this.cacheDirectory != null) {
            Util.deleteDirContents(new File(this.cacheDirectory));
        }
    }
}
