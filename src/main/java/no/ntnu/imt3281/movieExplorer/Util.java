package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * A class that contains generic helper methods, such as converting an array to
 * comma-separated string
 * 
 * @author Hakkon
 *
 */
public class Util {
    /**
     * Converts a comma-separated string to an ArrayList og strings
     * 
     * @param toConvert
     *            The string to convert
     * @return ArrayList of strings
     */
    public static ArrayList<String> stringToArray(String toConvert) {
        if (!toConvert.contains(","))
            return new ArrayList<String>();
        return new ArrayList<String>(Arrays.asList(toConvert.split(",")));
    }

    /**
     * Converts an ArrayList of strings to a comma-separated string
     * 
     * @param toConvert
     *            The ArrayList of strings to convert
     * @return A comma-separated string containing the strings in the array
     */
    public static String arrayToString(ArrayList<String> toConvert) {
        StringBuilder sb = new StringBuilder();

        if (toConvert.size() == 0)
            return "";

        for (int i = 0; i < toConvert.size(); i++) {
            sb.append(toConvert.get(i) + ",");
        }
        sb.deleteCharAt(sb.length() - 1);

        return sb.toString();
    }

    /**
     * Returns the combined size of all files contained within one directory in the
     * filesystem. Uses the Files API from Java 8, with its walk stream. Used for
     * listing size of cache directories.
     * 
     * @return File size in KB
     */
    public static long getDirectorySize(File directory) {
        try {
            long sizeInBytes = Files.walk(Paths.get(directory.toURI())).filter(Files::isRegularFile)
                    .mapToLong(path -> path.toFile().length()).sum();
            return (long) (sizeInBytes / Math.pow(2, 10));
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Deletes all files in the specified directory. Used for clearing the cache
     * directories.
     * 
     * @param directory
     *            The directory to delete files from.
     */
    public static void deleteDirContents(File directory) {
        try {
            Files.walk(Paths.get(directory.toURI())).filter(Files::isRegularFile).forEach((path) -> {
                path.toFile().delete();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
