package no.ntnu.imt3281.movieExplorer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Genres {
    private static String fromDB(long id) {
        Connection conn = Database.getConnection();

        String sql = "SELECT name FROM genres WHERE id=" + Long.toString(id);

        try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {

            if (rs.next()) {
                return rs.getString(1);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void toDB(long id, String name) {
        Connection conn = Database.getConnection();

        String sql = "INSERT INTO genres (id, name) VALUES(?, ?)";

        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.setString(2, name);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve the name of a movie or tv genre. If the genre already exists in the
     * database, it is taken from there. If not, it is requested from the API and
     * then added to the database.
     * 
     * @param genreID The id of the genre
     * @return The name of the genre
     */
    public static String resolve(long genreID) {
        String res = fromDB(genreID);
        if (res != null)
            return res;

        ArrayList<JSON> genres = new ArrayList<>(2);
        genres.add(API.sendRequest("genre/movie/list?").get("genres"));
        genres.add(API.sendRequest("genre/tv/list?").get("genres"));

        for (JSON genreList : genres) {
            for (int i = 0; i < genreList.size(); i++) {
                JSON genre = genreList.get(i);

                if ((long) genre.getValue("id") == genreID) {
                    toDB((long)genre.getValue("id"), (String) genre.getValue("name"));
                    return (String) genre.getValue("name");
                }
            }
        }

        return null;
    }
}
