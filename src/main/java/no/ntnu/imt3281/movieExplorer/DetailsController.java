package no.ntnu.imt3281.movieExplorer;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * This is the controller for the "detailPane" in GUI.fxml, to display details
 * about a movie
 * 
 * @author Hakkon
 *
 */
public class DetailsController {

    @FXML
    private ImageView movieImage;

    @FXML
    private TextArea movieText;

    @FXML
    private Label movieName;

    @FXML
    private Label movieGenres;

    private JSON movie;
    private TheMovieDBConfiguration movieDBConfig;

    /**
     * Create a new instance of DetailsController, takes a movie ID as parameter and
     * retrieves details about it from the Search class movie() method
     * 
     * @param movieID
     *            The ID of the movie to load the controller
     */
    public DetailsController(long movieID, TheMovieDBConfiguration config) {
        this.movie = Search.movie(movieID);
        this.movieDBConfig = config;
    }

    /**
     * This method is called when all JavaFX components are available and ready to
     * be used. Used to initialize components with details about the movie.
     */
    @FXML
    public void initialize() {
        String title = (String) movie.getValue("title");
        String overview = (String) movie.getValue("overview");
        String image = (String) movie.getValue("poster_path");
        
        // Title and overview
        this.movieName.setText(title);
        if (overview != null)
            this.movieText.setText(overview);
        else
            this.movieText.setText("Information not available.");

        // Genres
        JSON genres = movie.get("genres");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < genres.size(); i++) {
            sb.append(genres.get(i).getValue("name") + ", ");
        }
        if (sb.length() > 3)
            sb.delete(sb.length() - 2, sb.length());
        this.movieGenres.setText(sb.toString());
        
        // Image
        if (image != null) {
            Image poster = new Image(movieDBConfig.getPosterURL(image));
            this.movieImage.setImage(poster);
        }
    }
}