package no.ntnu.imt3281.movieExplorer;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class API {
    private static final String APIKEY = "2db8e7aefc8176af4140ae8d3aef636d";

    /**
     * Sends a request to TheMovieDB API
     * 
     * @param request
     *            The request to send
     * @return Response as JSON object
     */
    public static JSON sendRequest(String request) {
        String req = null;

        try {
            req = Unirest.get("https://api.themoviedb.org/3/" + request + "language=en-US&api_key=" + APIKEY).asString()
                    .getBody();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return new JSON(req);
    }
}
