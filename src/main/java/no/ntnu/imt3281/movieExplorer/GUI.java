package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.prefs.Preferences;

import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import no.ntnu.imt3281.movieExplorer.GUI.SearchResultItem;

public class GUI {
    @FXML
    private Pane detailPane;
    @FXML
    private TextField searchField;
    @FXML
    private TreeView<SearchResultItem> searchResult;
    @FXML
    private MenuItem preferences;
    @FXML
    private MenuItem about;

    // Root node for search result tree view
    private TreeItem<SearchResultItem> searchResultRootNode = new TreeItem<SearchResultItem>(new SearchResultItem(""));

    private TheMovieDBConfiguration movieDBConfig;

    /**
     * Retrieve configuration information once, and pass it to the movie detail
     * controllers
     */
    public GUI() {
        movieDBConfig = new TheMovieDBConfiguration();
    }

    @FXML
    /**
     * Called when the object has been created and connected to the fxml file. All
     * components defined in the fxml file is ready and available.
     */
    public void initialize() {
        searchResult.setRoot(searchResultRootNode);

        // Add change listener to TreeView elements
        searchResult.getSelectionModel().selectedItemProperty()
                .addListener((ObservableValue<? extends TreeItem<SearchResultItem>> observable,
                        TreeItem<SearchResultItem> oldValue, TreeItem<SearchResultItem> newValue) -> {
                    this.selectedSearchResult(newValue);
                });
    }

    @FXML
    /**
     * Called when the seqrch button is pressed or enter is pressed in the
     * searchField. Perform a multiSearch using theMovieDB and add the results to
     * the searchResult tree view.
     * 
     * @param event
     *            ignored
     */
    void search(ActionEvent event) {
        JSON result = Search.multiSearch(searchField.getText()).get("results");
        TreeItem<SearchResultItem> searchResults = new TreeItem<>(
                new SearchResultItem("Searching for : " + searchField.getText()));
        searchResultRootNode.getChildren().add(searchResults);
        for (int i = 0; i < result.size(); i++) {
            SearchResultItem item = new SearchResultItem(result.get(i));
            searchResults.getChildren().add(new TreeItem<SearchResultItem>(item));
        }
        searchResultRootNode.setExpanded(true);
        searchResults.setExpanded(true);
    }

    /**
     * Called when a search result is selected. This function is called by
     * ChangeListener lambda in initialize. Uses the search methods in class Search
     * to perform searches for movies and persons, and adds them to the selected
     * element.
     * 
     * @param selected
     *            A reference to the selected TreeItem
     */
    private void selectedSearchResult(TreeItem<SearchResultItem> selected) {
        SearchResultItem item = selected.getValue();

        // If selected item is person
        if (item.media_type.equals("person")) {
            // Get movies this person took part in
            JSON result = Search.takesPartIn(item.id).get("results");

            for (int i = 0; i < result.size(); i++) {
                SearchResultItem movie = new SearchResultItem(result.get(i), "movie");
                selected.getChildren().add(new TreeItem<SearchResultItem>(movie));
            }
        } else if (item.media_type.equals("movie")) {

            // Show details about this movie
            this.showMovieDetail(item.id);

            // Get persons who took part in this movie
            JSON result = Search.actors(item.id);
            JSON cast = result.get("cast");
            JSON crew = result.get("crew");

            // Add persons from both cast and crew array to node
            SearchResultItem person;
            for (int i = 0; i < cast.size(); i++) {
                person = new SearchResultItem(cast.get(i), "person");
                selected.getChildren().add(new TreeItem<SearchResultItem>(person));
            }

            for (int i = 0; i < crew.size(); i++) {
                person = new SearchResultItem(crew.get(i), "person");
                selected.getChildren().add(new TreeItem<SearchResultItem>(person));
            }
        }

    }

    /**
     * This method shows details about a specific movie in the "detailPane" pane.
     * 
     * @param id
     */
    private void showMovieDetail(long id) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Details.fxml"));
            DetailsController controller = new DetailsController(id, this.movieDBConfig);
            loader.setController(controller);

            // If the pane has no children, add new node, else replace the old one
            if (detailPane.getChildren().size() == 0)
                detailPane.getChildren().add(loader.load());
            else
                detailPane.getChildren().set(0, loader.load());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens a DirectoryChooser and allows the user to select the directory which is
     * used for caching images
     * 
     * @param event
     */
    @FXML
    void preferences(ActionEvent event) {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Mappe for bildecache");
        dirChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        File selectedDir = dirChooser.showDialog(null);
        System.out.println(selectedDir);

        // Directory selected, store the preference
        if (selectedDir != null) {
            Preferences pref = Preferences.userRoot().node("MovieExplorer");
            pref.put("CacheDirectory", selectedDir.toString());

            // Create directories
            String dirs[] = { "/w1280", "/w500", "/w780", "/h623", "/w300" };
            for (int i = 0; i < dirs.length; i++) {
                File dir = new File(selectedDir.toString() + dirs[i]);
                dir.mkdir();
            }
        }
    }

    /**
     * Opens an "about" dialog where an overview of disk space used by the
     * application can be seen and the option to clear database or cache.
     * 
     * @param event
     * @throws Exception 
     */
    @FXML
    void aboutDialog(ActionEvent event) throws Exception {
        Dialog<?> aboutDialog = new Dialog<>();
        aboutDialog.setTitle("Om MovieExplorer");
        aboutDialog.setWidth(200);
        aboutDialog.setHeight(300);
        
        Label header = new Label("Om MovieExplorer");
        header.setFont(Font.font(18));
        Label diskUsage = new Label("Diskforbruk:");
        
        VBox box = new VBox();
        box.setSpacing(5);
        
        box.getChildren().add(header);
        box.getChildren().add(diskUsage);
        
        Label[] usageLabels = new Label[6];
        for (int i = 0; i < usageLabels.length; i++) {
            usageLabels[i] = new Label("Label" + i);
            box.getChildren().add(usageLabels[i]);
        }
        
        // Update disk usage labels lambda
        Runnable updateLabels = () -> {
            usageLabels[0].setText("Databasen\t\t\t" + Long.toString(Database.sizeOfDatabase()) + " KB");
            usageLabels[1].setText("Posterbilder\t\t\t" + Long.toString(movieDBConfig.sizeOfCache("w780")) + " KB");
            usageLabels[2].setText("Profilbilder\t\t\t" + Long.toString(movieDBConfig.sizeOfCache("h623")) + " KB");
            usageLabels[3].setText("Backdropbilder\t\t" + Long.toString(movieDBConfig.sizeOfCache("w1280")) + " KB");
            usageLabels[4].setText("Logobilder\t\t\t" + Long.toString(movieDBConfig.sizeOfCache("w500")) + " KB");
            usageLabels[5].setText("Stillbilder\t\t\t\t" + Long.toString(movieDBConfig.sizeOfCache("w300")) + " KB");
        };
        updateLabels.run();
        
        Button emptyDatabase = new Button("Tøm database");
        emptyDatabase.setOnAction(e -> {
            Database.emptyDatabaseCache();
            updateLabels.run();
        });
        Button emptyCache = new Button("Tøm cache");
        emptyCache.setOnAction(e -> {
            movieDBConfig.emptyCache();
            updateLabels.run();
        });
        
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        box.getChildren().add(hBox);
        hBox.getChildren().add(emptyDatabase);
        hBox.getChildren().add(emptyCache);
        
        aboutDialog.getDialogPane().getButtonTypes().add(new ButtonType("OK", ButtonData.OK_DONE));
        aboutDialog.getDialogPane().setContent(box);
        aboutDialog.show();
    }

    class SearchResultItem {
        private String media_type = "";
        private String name = "";
        private long id;
        private String profile_path = "";
        private String title = "";

        /**
         * Create new SearchResultItem with the given name as what will be displayed in
         * the tree view.
         * 
         * @param name
         *            the value that will be displayed in the tree view
         */
        public SearchResultItem(String name) {
            this.name = name;
        }

        /**
         * Create a new SearchResultItem with data form this JSON object.
         * 
         * @param json
         *            contains the data that will be used to initialize this object.
         */
        public SearchResultItem(JSON json) {
            media_type = (String) json.getValue("media_type");

            if (media_type.equals("person")) {
                name = (String) json.getValue("name");
                profile_path = (String) json.getValue("profile_path");
            } else if (media_type.equals("movie")) {
                title = (String) json.getValue("title");
            } else {
                name = (String) json.getValue("name");
            }
            id = (Long) json.getValue("id");
        }

        /**
         * Create a new SearchResultItem with specified type and data from JSON object.
         * Need this because some JSON used in searches don't contain the media_type
         * field
         * 
         * @param json
         *            The JSON data that will initialize this object
         * @param type
         *            The media type, can be "person", "movie" or "tv"
         */
        public SearchResultItem(JSON json, String type) {
            media_type = type;
            id = (long) json.getValue("id");

            if (media_type.equals("movie")) {
                title = (String) json.getValue("title");
            } else if (media_type.equals("person")) {
                name = (String) json.getValue("name");
            }
        }

        /**
         * Used by the tree view to get the value to display to the user.
         */
        @Override
        public String toString() {
            if (media_type.equals("person")) {
                return name;
            } else if (media_type.equals("movie")) {
                return title;
            } else {
                return name;
            }
        }
    }

}
