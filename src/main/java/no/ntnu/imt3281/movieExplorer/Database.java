package no.ntnu.imt3281.movieExplorer;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

/**
 * This class is responsible for managing the database. When the class is
 * instantiated it checks if the database already exists, if it doesn't a new
 * database is created and tables created.
 * 
 * @author Hakkon
 *
 */
public class Database {
    private static final String DBNAME = "movieExplorerDB";
    private static final String DATABASE = "jdbc:derby:" + DBNAME;
    private static Connection conn;

    static {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (ClassNotFoundException e) {
            System.err.println("Could not find JDBC driver");
            e.printStackTrace();
        }

        // Try to get a connection
        try {
            conn = DriverManager.getConnection(DATABASE);
        } catch (SQLException e) {
            // Database doesn't exist, so create
            try {
                conn = DriverManager.getConnection(DATABASE + ";create=true");
                createDB();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    private static void createDB() throws SQLException {
        Statement stmt = conn.createStatement();

        // Genres table
        String sql = "CREATE TABLE genres (" + "id BIGINT NOT NULL," + "name VARCHAR(64) NOT NULL," + "PRIMARY KEY(id)"
                + ")";
        stmt.execute(sql);

        // takesPartIn cache
        sql = "CREATE TABLE takesPartIn (" + "id BIGINT NOT NULL," + "json LONG VARCHAR," + "PRIMARY KEY(id))";
        stmt.execute(sql);

        // actors cache
        sql = "CREATE TABLE actors (" + "id BIGINT NOT NULL," + "json LONG VARCHAR," + "PRIMARY KEY(id))";
        stmt.execute(sql);

        // movies cache
        sql = "CREATE TABLE movies (" + "id BIGINT NOT NULL," + "json LONG VARCHAR," + "PRIMARY KEY(id))";
        stmt.execute(sql);

        // Configuration table
        sql = "CREATE TABLE config (" + "id INT NOT NULL," + "baseURL VARCHAR(64) NOT NULL,"
                + "secureBaseURL VARCHAR(64) NOT NULL," + "backdropSizes VARCHAR(256) NOT NULL,"
                + "logoSizes VARCHAR(256) NOT NULL," + "posterSizes VARCHAR(256) NOT NULL,"
                + "profileSizes VARCHAR(256) NOT NULL," + "stillSizes VARCHAR(256) NOT NULL," + "PRIMARY KEY(id))";
        stmt.execute(sql);
    }

    /**
     * Returns a column from the database and puts every attribute into a hashmap
     * 
     * @param table
     *            The name of the table where the column is
     * @param pk
     *            The primary key of the column to fetch
     * @return HashMap with every attribute/data contained in the column
     */
    public static HashMap<String, Object> getColumn(String table, String pk) {

        String sql = "SELECT * FROM " + table + " WHERE id=" + pk;

        try (Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {

            if (rs.next()) {
                ResultSetMetaData md = rs.getMetaData();
                int colCount = md.getColumnCount();
                HashMap<String, Object> result = new HashMap<>(colCount);

                for (int i = 1; i <= colCount; i++) {
                    result.put(md.getColumnName(i), rs.getObject(i));
                }
                return result;
            } else {
                return null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Returns a connection to the database
     * 
     * @return Connection a connection to the database
     */
    public static Connection getConnection() {
        return conn;
    }

    /**
     * Deletes all rows from the search cache tables.
     */
    public static void emptyDatabaseCache() {
        String sql1 = "TRUNCATE TABLE takesPartIn";
        String sql2 = "TRUNCATE TABLE actors";
        String sql3 = "TRUNCATE TABLE movies";
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql1);
            stmt.executeUpdate(sql2);
            stmt.executeUpdate(sql3);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Returns the size of the database in KB.
     * 
     * @return Size of database
     */
    public static long sizeOfDatabase() {
        File databaseDir = new File(DBNAME);
        return Util.getDirectorySize(databaseDir);
    }
}
